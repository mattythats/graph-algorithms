#ifndef GRAPH_H
#define GRAPH_H

#include <iostream>
#include <vector> //for adjlist
#include <queue> //for BFS
#include <map> //for everything
#include <stack> //for topological ordering
#include <fstream> //for reading from a file
#include <list> //for cover
#include <chrono> //for cover
#include <limits> //for SP/MST algos

//this class represents an undirected graph
class Graph{
	enum color_t{
		WHITE,GREY,BLACK
	};
	private:
		bool dir = false; //directed or not, decided on instantiation
		bool wgt = false; //weighted edges or not, decided on instantiation
		bool acyc; //acyclic or not, decided when it needs to be known
		std::map<int,std::vector<int>> edges; //adjacency list
		std::map<int,std::vector<int>> weight; //weight list weight @ map.vector[i] is the weight of edge (map,vector[i])

		void DFSVisit(int,int&,std::map<int,color_t>&,std::map<int,int>&,std::map<int,std::pair<int,int>>&); //recursive dfs helper
		void topological(int,std::map<int,color_t>&,std::stack<int>&); //recursive topological sort
		void SCCVisit(int,std::map<int,color_t>&,std::stack<int>&); //recursive modified DFS for SCC
		void SCCVisitR(int,std::map<int,color_t>&,std::map<int,int>&); //recursive modified DFS for SCC on the transpose
		bool isDAG(); //is the graph directed and acyclic
		int findK(std::map<int,int>&,int);//union-find helper for kruskals
		void unionK(std::map<int,int>&,int,int);//union-find helper for kruskals
	public:
		Graph();
		Graph(bool,bool = false); //constructor
		Graph(const Graph&); //copy constructor returns a transpose of the graph
		void addVertex(int); //adds a vertex to the graph
		void addEdge(int,int,int = 1); //adds an edge to the graph with weight w
		void readFile(std::string filename); //reads an adjacency list from a graph
		void print(); //prints the graph + adjacency list
		void printBFS(int); //prints the result of a breadth-first search starting from node k
		void DFS(int); //performs a depth first search on the graph
		void SCC(); //prints all strongly connected components of the graph
		void cover(bool); //finds a cover of the graph
		void dijkstra(int,int); //finds the shortest path from node A to node B greedily
		void bellman_ford(int,int = -1); //finds the shorests path from node A to all nodes by relaxing every edge
		void floyd_warshall(int,int=-1);
		void prim(int=-1);
		void kruskal();
};

#endif
