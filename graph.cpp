#include "graph.h"

//default constructor
//defaults to undirected graph
Graph::Graph(){
	dir = false;
	wgt = false;
}

//value constructor
//decides if the graph is directed or not
Graph::Graph(bool d, bool w){
	dir = d;
	wgt = w;
}

//copy constructor
//returns a transpose of g
Graph::Graph(const Graph& g){
	dir = g.dir;
	wgt = g.wgt;
	for(auto it = g.edges.begin(); it != g.edges.end(); ++it){
		for(auto itL = it->second.begin(); itL != it->second.end(); ++itL){
			int d = std::distance(it->second.begin(),itL);
			addEdge(*itL,it->first,g.weight.find(it->first)->second[d]);
		}
	}
}

//adds a vertex + empty adjacency list to the graph
void Graph::addVertex(int n){
	std::vector<int> v;
	std::pair<int,std::vector<int>> p = std::make_pair(n,v);
	edges.insert(p);
	weight.insert(p);
}

//adds an edge between two verticies, adds requisite verticies if they're not in the graph
void Graph::addEdge(int n, int m, int w){
	addVertex(n);
	addVertex(m);
	bool dupe = false;
	if(wgt == false){
		w = 1;
	}
	std::vector<int> v1 = edges.find(n)->second; //take adjList for n
	std::vector<int>& w1 = weight.find(n)->second; //weight list
	for(int i = 0; i < v1.size(); i++){
		if(v1[i] == m){ //if m is already in adjlist...
			dupe = true; //dont add it again
			w1[i] = w;
			break;
		}
	}
	if(dupe == false){ //if m is not in adjlist add it to n's adjList
		edges.find(n)->second.push_back(m);
		weight.find(n)->second.push_back(w);
		if(dir == false && m != n){ //if its not a directed graph put n into m's adjList
			edges.find(m)->second.push_back(n);
			weight.find(m)->second.push_back(w);
		}
	} else {
		if(dir == false && m!=n){
			std::vector<int> v2 = edges.find(m)->second;
			std::vector<int>& w2 = weight.find(m)->second;
			for(int i = 0; i < v2.size(); i++){
				if(v2[i] == n){
					w2[i] = w;
				}
			}
		}
	}
}

//reads an adjacency list from a file and puts it into the graph.
//expects a format A B C D E where A, B, C, D, E, ... are vertexes (ints) in the graph and the edges are
//(A,B) (A,C) (A,D) (A,E) ...
//TODO: read weighted graph
void Graph::readFile(std::string filename){
	std::cout << filename << std::endl;
	std::fstream file;
	file.open(filename);
	if(file.is_open()){ //if there is a file named filename
		while(!file.eof()){ //read thru it
			std::string s;
			getline(file,s); //take a line
			if(s.length() > 2){ //if it has at least one edge
				int u = std::stoi(s.substr(0,s.find(" "))); //take the first number
				s = s.substr(s.find(" ")+1); //chop it off
				std::string c; //control for the while loop
				while(c != s){
					c = s; //c is s
					int v = std::stoi(s.substr(0,s.find(" "))); //take the first number in the string
					s = s.substr(s.find(" ")+1); //chop it off s
					addEdge(u,v); //add the edge
				}
			}
		}
	} else {
		std::cout << "File does not exist." << std::endl;
	}
	file.close();
}

//prints the graph + adjacency list
void Graph::print(){
	std::cout << "This graph is ";
	if(dir){
		std::cout << "a directed graph." << std::endl;
	} else {
		std::cout << "an undirected graph." << std::endl;
	}
	std::cout << "This graph is ";
	if(wgt){
		std::cout << "a weighted graph." << std::endl;
	} else {
		std::cout << "an unweighted graph." << std::endl;
	}
	DFS(2);
	std::cout << "This graph ";
	if(acyc){
		std::cout << "does not have a cycle." << std::endl;
	} else {
		std::cout << "has at least one cycle." << std::endl;
	}
	std::cout << "\nAdjacency List" << std::endl;
	for(auto it = edges.begin(); it != edges.end(); ++it){
		std::cout << it->first << ": ";
		for(auto itL = it->second.begin(); itL != it->second.end(); ++itL){
			if(wgt){
				int d = std::distance(it->second.begin(),itL);
				std::cout << *itL << "|" << weight.find(it->first)->second[d] << ", ";
			} else {
				std::cout << *itL << ", ";
			}
		}
		std::cout << std::endl;
	}
	std::cout << std::endl;
}

//performs a breadth first search starting at node n and outputs results
void Graph::printBFS(int n){
	std::cout << "\nBFS Traversal starting at " << n << std::endl << std::endl;
	if(edges.find(n) != edges.end()){
		std::map<int,color_t> colors; //for not started/started/finished
		std::map<int,int> parents; //parents
		std::map<int,int> distance; //distance from source node
		std::queue<int> q; //controls the traversal
		for(auto it = edges.begin(); it != edges.end(); ++it){ //initialize the maps
			colors.insert(std::make_pair(it->first,WHITE)); //white means not started
			parents.insert(std::make_pair(it->first,-1)); //a parent of -1 indicates it hasn't been traversed
			distance.insert(std::make_pair(it->first,-1)); //a distance of -1 indicates it hasn't been traversed
		}
		colors.find(n)->second = GREY; //grey means in progress
		distance.find(n)->second = 0; //sets distance to 0 because its the starting node
		q.push(n); //puts n in the queue
		while(!q.empty()){ //while there are nodes in the queue
			int u = q.front(); //u is the node in front
			q.pop(); //takes u out of the queue
			std::cout << u << "->"; //prints to illustate path
			std::vector<int> adjList = edges.find(u)->second; //take the adjacency list of u
			for(int v = 0; v < adjList.size(); v++){ //for every node attached to u
				if(colors.find(adjList[v])->second == WHITE){ //if it hasnt started yet
					colors.find(adjList[v])->second = GREY; //start it
					distance.find(adjList[v])->second = distance.find(u)->second+1; //its 1 away from u
					parents.find(adjList[v])->second = u; //its parent is u
					q.push(adjList[v]); //add it to the queue
				}
				colors.find(u)->second = BLACK; //black means done
			}
		}
		std::cout << "end" << std::endl;
		//print each node, its parent, and its distance from the start node
		std::cout << "Child\tParent\tDistance" << std::endl;
		for(auto it = parents.begin(); it != parents.end(); ++it){
			std::cout << it->first << "\t" << it->second << "\t" << distance.find(it->first)->second << std::endl;
		}
	} else {
		std::cout << "Sorry, either " << n << " is not in the graph or has no edges." << std::endl;
	}
}

//performs a (modified) DFS depending on the version parameter
void Graph::DFS(int version){
	//version just gives us options for the operations springing from DFS
	//version = 0 is your standard DFS + printing results
	//version = 1 is topological ordering
	//version = 2 is DFS without printing
	std::map<int,color_t> colors; //for not started/started/finished
	std::map<int,int> parents; //discovered from node[p]
	std::map<int,std::pair<int,int>> times; //(start,finish) times
	std::stack<int> s; //controls the traversal for topological
	for(auto it = edges.begin(); it != edges.end(); ++it){ //init maps
			colors.insert(std::make_pair(it->first,WHITE));
			parents.insert(std::make_pair(it->first,-1));
			times.insert(std::make_pair(it->first,std::make_pair(-1,-1)));
	}
	int time = 0; //global time variable for discovery/finish time
	for(auto it = edges.begin(); it != edges.end(); ++it){
			if(colors.find(it->first)->second == WHITE){
				if(version == 0 || version == 2){ //perform a dfs
					DFSVisit(it->first,time,colors,parents,times);
				} else if(version == 1){
					topological(it->first,colors,s);
					break; //prints |v| times if not a dag otherwise
				}
			}
	}
	if(version == 0){ //prints DFS results
		std::cout << "\nDFS Traversal" << std::endl;
		std::cout << "\nNodes\tParents\t(Discovered,Finished)\n";
		for(auto it = edges.begin(); it != edges.end(); ++it){
				std::cout << it->first << "\t" << parents.find(it->first)->second << "\t(" << times.find(it->first)->second.first << "," << times.find(it->first)->second.second << ")\n";
		}
	} else if(version == 1 && isDAG()){ //prints topologial ordering if called to do so
		std::cout << "\nTopological ordering" << std::endl;
		while(!s.empty()){
			std::cout << s.top() << ", ";
			s.pop();
		}
		std::cout << std::endl;
	}
}

//DFS helper function
void Graph::DFSVisit(int node, int& time, std::map<int,color_t>& colors, std::map<int,int>& parents, std::map<int,std::pair<int,int>>& times){
	int s = time++; //set time found, increase time by 1
	colors.find(node)->second = GREY; //grey means in progress
	std::vector<int> adjList = edges.find(node)->second; //take the adjacency list
	for(int i = 0; i < adjList.size(); i++){ //for every node in the adjacency list...
		if(colors.find(adjList[i])->second == WHITE){ //if its white
			parents.find(adjList[i])->second = node; //set node as its parent...
			DFSVisit(adjList[i],time,colors,parents,times); //and run dfs on it
		}else if(colors.find(adjList[i])->second == GREY){ //if its grey...
			acyc = false; //this graph has a cycle
		}
	}
	colors.find(node)->second = BLACK; //black means done
	int e = time++; //set time finished, increase time by 1
	times.find(node)->second = std::make_pair(s,e); //put the start/finish pair in the map
}

//performs a topological sort on the graph using a modified DFS
void Graph::topological(int node, std::map<int,color_t>& colors, std::stack<int>& s){
	if(isDAG()){ //if the graph is directed and acyclic
		colors.find(node)->second = GREY; //grey means a node is in progress
		std::vector<int> adjList = edges.find(node)->second; //take the adjList
		for(int i = 0; i < adjList.size(); i++){
			if(colors.find(adjList[i])->second == WHITE){ //if a node hasn't been started
				topological(adjList[i],colors,s); //recursively discover it
			}
		}
		colors.find(node)->second = BLACK; //finish it
		s.push(node); //put it in the stack, this successfully orders nodes in order of decreasing finish times
	} else {
		std::cout << "This is not a DAG therefore there is no topological order" << std::endl;
	}
}

void Graph::SCC(){
	std::map<int,color_t> colors; //for not started/started/finished
	std::map<int,int> parents; //for the printing bit really
	std::stack<int> s; //controls the traversal for the reversed graph
	for(auto it = edges.begin(); it != edges.end(); ++it){ //init maps
			colors.insert(std::make_pair(it->first,WHITE));
			parents.insert(std::make_pair(it->first,-1));
	}
	for(auto it = edges.begin(); it != edges.end(); ++it){
		if(colors.find(it->first)->second == WHITE){
			SCCVisit(it->first,colors,s); //populate stack
		}
	}
	Graph reversed(*this);
	for(auto it = edges.begin(); it != edges.end(); ++it){ //reset colors
			colors.find(it->first)->second = WHITE;
	}
	std::cout << "Strongly Connected Components" << std::endl << std::endl;
	while(!s.empty()){
		reversed.SCCVisitR(s.top(),colors,parents);
		if(parents.find(s.top())->second == -1){ //i kept getting end printed |V| times so I used this as a solution
			std::cout << "end" << std::endl;
		}
		s.pop();
	}
}

void Graph::SCCVisit(int node, std::map<int,color_t>& colors, std::stack<int>& s){
	colors.find(node)->second = GREY; //grey means a node is in progress
	std::vector<int> adjList = edges.find(node)->second; //take the adjList
	for(int i = 0; i < adjList.size(); i++){
		if(colors.find(adjList[i])->second == WHITE){ //if a node hasn't been started
			SCCVisit(adjList[i],colors,s); //recursively discover it
		}
	}
	colors.find(node)->second = BLACK; //finish it
	s.push(node); //put it in the stack, which successfully orders nodes by decreasing finish times
}

void Graph::SCCVisitR(int node, std::map<int,color_t>& colors, std::map<int,int>& parents){
	if(colors.find(node)->second == WHITE){ //if the node is white bc otherwise its already part of a SCC
		colors.find(node)->second = GREY; //grey means in progress
		std::vector<int> adjList = edges.find(node)->second; //take the adjacency list
		for(int i = 0; i < adjList.size(); i++){ //for every node in the adjacency list...
			if(colors.find(adjList[i])->second == WHITE){ //if its white
				parents.find(adjList[i])->second = node; //set its parent to the current node
				SCCVisitR(adjList[i],colors,parents); //and recurse
			}
		}
		colors.find(node)->second = BLACK; //black means done
		std::cout << node << "->"; //print
	}
}

void Graph::cover(bool r){
	std::vector<int> verts;
	std::list<std::pair<int,int>> edgeList;
	for(auto it = edges.begin(); it != edges.end(); ++it){
		for(auto itL = it->second.begin(); itL != it->second.end(); ++itL){
			edgeList.push_back(std::make_pair(it->first,*itL));
		}
	}
	if(r){
		std::srand(std::time(0)); //seeding the random generator
		while(edgeList.size() > 0){
			auto select = edgeList.begin();
			std::advance(select,rand()%edgeList.size());
			verts.push_back(select->first);
			verts.push_back(select->second);
			for(auto it = edgeList.begin(); it != edgeList.end(); ++it){
				for(int i = 0; i < verts.size(); i++){
					if(verts[i] == it->first || verts[i] == it->second){
						edgeList.remove(*it);
						it--;
					}//end if
				}//end for
			}//end iterator for
		}//end while
		std::cout << "Random cover: ";
	} else {
		while(edgeList.size() > 0){
			verts.push_back(edgeList.front().first);
			verts.push_back(edgeList.front().second);
			for(auto it = edgeList.begin(); it != edgeList.end(); ++it){
				for(int i = 0; i < verts.size(); i++){
					if(verts[i] == it->first || verts[i] == it->second){
						edgeList.remove(*it);
						it--;
					}//end if
				}//end verts for
			}//end iterator for
		}//end while
		std::cout << "Cover: ";
	}
	for(int i = 0; i < verts.size(); i++){
		std::cout << verts[i] << ", ";
	}
	std::cout << std::endl;
}

bool Graph::isDAG(){
	DFS(2);
	return dir && acyc;
}

//dijkstra's algorithm for finding the shortest paths in a graph
//works for undirected graphs with no weighted edges or negative edges
void Graph::dijkstra(int start, int end){
	bool neg = false;
	for(auto it = weight.begin(); it != weight.end(); ++it){
		for(auto itL = it->second.begin(); itL != it->second.end(); ++itL){
			if(*itL < 0){
				neg = true;
				break;
			}
		}
	}
	if(!neg){
		std::list<int> verts;
		std::map<int,int> dist;
		std::map<int,int> parents;
		for(auto it = edges.begin(); it != edges.end(); ++it){//init structures
			verts.push_back(it->first);//list of verticies
			dist.insert(std::make_pair(it->first,std::numeric_limits<int>::max()));//distance is max(int) so it can be replaced easily
			parents.insert(std::make_pair(it->first,-1));//-1 indicates it was never touched
		}
		dist.find(start)->second = 0;//start is 0 dist from itself
		while(verts.size() > 0){//while there are verticies in the list
			int minD = std::numeric_limits<int>::max();//so it can easily be replaced
			int u;//our vertex (guaranteed to be start on first run)
			for(auto it = verts.begin(); it != verts.end(); ++it){
				if(dist.find(*it)->second <= minD){//find the node with smallest dist in list of valid nodes
					minD = dist.find(*it)->second;
					u = *it;
				}
			}
			verts.remove(u);//remove the node with the smallest distance from the list
			std::vector<int> neighbors = edges.find(u)->second;
			for(int i = 0; i < neighbors.size(); i++){//for all neighboring nodes
				auto wIT = weight.find(u)->second.begin();//grab corresponding weight list
				std::advance(wIT,i);//move it so we have the weight of (u,neighbor[i]);
				int d = dist.find(u)->second + *wIT;//add the distance of (u,neighbor[i]) to the distance of the current node from start
				if(d < dist.find(neighbors[i])->second){//if new weight is smaller than the current weight of neigbors[i]
					dist.find(neighbors[i])->second = d;//replace it
					parents.find(neighbors[i])->second = u;//u is now the parent of neighbor[i]
				}//end if
			}//end for neighbors
		}//end while
		std::stack<int> s;
		int u = end;//starting at the node we're looking for
		while(u != -1){
			s.push(u);//push it into a stack (for printing)
			u = parents.find(u)->second;//bounce up to its parent so we can find the path
		}
		if(dist.find(end)->second == 0){
			std::cout << "There is no Dijkstra path from " << start << " to " << end << ".\n";
		} else {
			std::cout << "Dijkstra's path from " << start << " to " << end << ": ";
			while(s.size() > 0){
				std::cout << s.top() << "->";
				s.pop();
			}
			std::cout << "end. This path has a length of " << dist.find(end)->second << std::endl;
		}
	} else {
		std::cout << "This graph has at least one negative edge, therefore Dijkstra's algorithm cannot be performed." << std::endl;
	}
}

//bellman and ford's algorithm for finding shortest paths in a graph
//works for directed and undirected graphs with weighted edges but no negative cycles
//can be used to find negative cycles
void Graph::bellman_ford(int start, int end){
	std::map<int,int> dist; //distance map
	std::map<int,int> parent; //parent map
	bool negCyc = false;
	for(auto it = edges.begin(); it != edges.end(); ++it){//init maps
		dist.insert(std::make_pair(it->first,std::numeric_limits<int>::max()));
		parent.insert(std::make_pair(it->first,-1));
	}

	//relaxing edges
	dist.find(start)->second = 0;
	for(auto it = edges.begin(); it != --edges.end(); ++it){
		std::vector<int> neighbors = it->second;
		std::vector<int> w = weight.find(it->first)->second;
		for(int i = 0; i < neighbors.size(); i++){
			int d;//distance from current node
			if(dist.find(it->first)->second == std::numeric_limits<int>::max()){//if the distance of the v isn't defined
				d = w[i]; //then its the weight (u,v)
			} else {
				d = dist.find(it->first)->second + w[i];//otherwise it is the current weight + the weight of (u,v)
			}
			if(dist.find(neighbors[i])->second == std::numeric_limits<int>::max()){//if the distance isnt defined
				dist.find(neighbors[i])->second = d;//its d
			} else if(d < dist.find(neighbors[i])->second){//if it is defined and d is less than it
				dist.find(neighbors[i])->second = d;//its d
				parent.find(neighbors[i])->second = it->first;//u is the parent of v
			}//end if
		}//end for
	}//end for
	
	//finding negative cycles
	for(auto it = edges.begin(); it != edges.end(); ++it){
		std::vector<int> neighbors = it->second;
		std::vector<int> w = weight.find(it->first)->second;
		for(int i = 0; i < neighbors.size(); i++){
			int d = dist.find(it->first)->second + w[i];
			if(d < dist.find(neighbors[i])->second){//if d is shorter than the distance of v from start
				negCyc = true;//then we have a negative cycle
				break;
			}//end if
		}//end for
	}//end for
	
	//printing
	if(negCyc){//if theres a negative cycle
		std::cout << "There is no Bellman-Ford shortest path in this graph because there is a negative cycle." << std::endl;
	} else {
		if(end == -1){//if we just want all the distances from start
			std::cout << "Bellman-Ford distances from " << start << std::endl;
			for(auto it = dist.begin(); it != dist.end(); ++it){
				std::cout << it->first << ": " << it->second << std::endl;
			}//end for
		} else {//print the path
			std::stack<int> s;
			int c = end;
			while(c != -1){
				s.push(c);
				c = parent.find(c)->second;
			}
			std::cout << "The Bellman-Ford path from " << start << " to " << end << " is : ";
			while(s.size() > 0){
				std::cout << s.top() << "->";
				s.pop();
			}
			std::cout << "end" << std::endl;
		}
	}
}

//TODO: add path reconstruction maybe
void Graph::floyd_warshall(int start, int end){
	//creating the distance matrix
	int** dist = new int*[edges.size()];
	int** next = new int*[edges.size()];
	for(int i = 0; i < edges.size(); i++){
		dist[i] = new int[edges.size()];
		next[i] = new int[edges.size()];
	}
	for(int i = 0; i < edges.size(); i++){
		for(int j = 0; j < edges.size(); j++){
			dist[i][j] = std::numeric_limits<int>::max();
			next[i][j] = -1;
		}
	}
	//seeding the distance matrix
	for(auto it = edges.begin(); it != edges.end(); ++it){
		std::vector<int> neighbors = it->second;
		std::vector<int> weights = weight.find(it->first)->second;
		int i = std::distance(edges.begin(),it);
		for(auto itL = neighbors.begin(); itL != neighbors.end(); ++itL){
			int j = std::distance(edges.begin(),edges.find(*itL));
			dist[i][j] = weights[std::distance(neighbors.begin(),itL)];
			next[i][j] = *itL;
		}
	}
	for(int i = 0; i < edges.size(); i++){
		dist[i][i] = 0;
	}
	//doing the algorithm
	for(int k = 0; k < edges.size(); k++){
		for(int i = 0; i < edges.size(); i++){
			for(int j = 0; j < edges.size(); j++){
				if(dist[i][k] != std::numeric_limits<int>::max() && dist[k][j] != std::numeric_limits<int>::max() && dist[i][j] > dist[i][k] + dist[k][j]){
					dist[i][j] = dist[i][k]+dist[k][j];
					next[i][j] = next[i][k];
				}//end if
			}//end j
		}//end i
	}//end k
	
	//printing the final matrix
	std::cout << "FLOYD WARSHALL DISTANCE MATRIX" << std::endl;
	std::cout << std::endl << "  ";
	for(auto it = edges.begin(); it != edges.end(); ++it){
		std::cout << it->first << "\t";
	}
	std::cout << std::endl << "  ";
	for(auto it = edges.begin(); it != edges.end(); ++it){
		std::cout << "-" << "\t";
	}
	//printing distance matrix
	std::cout << std::endl;
	auto itP = edges.begin();
	for(int i = 0; i < edges.size(); i++){
		std::cout << itP->first << "| ";
		for(int j = 0; j < edges.size(); j++){
			if(dist[i][j] == std::numeric_limits<int>::max()){
				std::cout << "inf" << "\t";
			} else{
				std::cout << dist[i][j] << "\t";
			}
		}
		itP++;
		std::cout << std::endl;
	}
	//printing next
	std::cout << std::endl << "FLOYD-WARSHALL NEXT MATRIX" << std::endl;
	for(int i = 0; i < edges.size(); i++){
		for(int j = 0; j < edges.size(); j++){
			std::cout << next[i][j] << " ";
		}
		std::cout << std::endl;
	}
	//detecting negative cycles
	bool negCyc = false;
	for(int i = 0; i < edges.size(); i++){
		if(dist[i][i] < 0){
			negCyc = true;
			break;
		}
	}
	if(negCyc){
		std::cout << "This graph contains at least one negative cycle." << std::endl;
	}
	//delete dist and next
	for(int i = 0; i < edges.size(); i++){
		delete dist[i];
		delete next[i];
	}
	delete dist;
	delete next;
}

void Graph::prim(int s){
	std::srand(std::time(0));
	std::map<int,color_t> verts;//list of verticies
	std::map<int,int> costs;//lightest edge connected to cost[i]
	std::map<int,int> edge; //which vertex is on the other side of the lightest link
	Graph mst(dir,wgt); //mst graph
	for(auto it = edges.begin(); it != edges.end(); ++it){
		verts.insert(std::make_pair(it->first,WHITE)); //white means its not in the MST
		costs.insert(std::make_pair(it->first,std::numeric_limits<int>::max())); //max means it hasnt been looked at
		edge.insert(std::make_pair(it->first,-1));//-1 means it hasnt been looked at
	}
	if(s == -1){//if no vertex is initially selected then chose a random one
		int r = rand()%edges.size();
		auto it = edges.begin();
		std::advance(it,r);
		s = it->first;
	}
	costs.find(s)->second = 0;
	bool wht = true;
	while(wht){//while there are verticies not selected yet
		int u;
		int minCost = std::numeric_limits<int>::max();
		for(auto it = costs.begin(); it != costs.end(); ++it){
			if(verts.find(it->first)->second == WHITE && it->second <= minCost){ //chose the vertex with the lightest edge
				u = it->first;
				minCost = it->second;
			}//end if
		}//end find min for loop
		verts.find(u)->second = BLACK;//remove it from the selection pool
		if(verts.find(edge.find(u)->first)->second == WHITE && edge.find(u)->second != -1){
			verts.find(edge.find(u)->first)->second = BLACK;//if theres another vertex attached then remove that as well
		}//end edge if
		std::vector<int> neighbors = edges.find(u)->second;
		std::vector<int> weights = weight.find(u)->second;
		for(int i = 0; i < neighbors.size(); i++){//loop through all edges connected to the currently selected vertex
			int v = neighbors[i];
			int w = weights[i];
			if(w < costs.find(v)->second){ //if there is a lighter edge than currently indicated
				costs.find(v)->second = w;//change it to this one
				edge.find(v)->second = u;
			}//end if
		}//end for
		wht = false;
		for(auto it = verts.begin(); it != verts.end(); ++it){//check to see if verticies can still be selected
			if(it->second == WHITE){
				wht = true;
				break;
			}//end if
		}//end for
	}//end while
	for(auto it = edges.begin(); it != edges.end(); ++it){//build the MST graph
		if(edge.find(it->first)->second != -1){
			mst.addEdge(edge.find(it->first)->second,it->first,costs.find(it->first)->second);
		} else {//for the root node add the lightest edge connected to it if there isnt one selected
			std::vector<int> neighbors = it->second;
			std::vector<int> weights = weight.find(it->first)->second;
			int u = -1;
			int w = std::numeric_limits<int>::max();
			for(int i = 0; i < neighbors.size(); i++){
				if(weights[i] < w){
					u = neighbors[i];
					w = weights[i];
				}//end if
			}//end for
			mst.addEdge(u,it->first,w);
		}//end if
	}//end for
	std::cout << "\nPrim's Algorithm for MST rooted at " << s << std::endl;
	mst.print();
}

void Graph::kruskal(){
	std::map<int,int> parent; //for the disjoint set system
	std::list<std::tuple<int,int,int>> edgeList;//list of edges
	Graph mst(dir,wgt);
	for(auto it = edges.begin(); it != edges.end(); ++it){
		parent.insert(std::make_pair(it->first,-1));//-1 means its the head of the set
		std::vector<int> neighbors = it->second;
		std::vector<int> weights = weight.find(it->first)->second;
		for(int i = 0; i < neighbors.size(); i++){
			//(weight,second node, first node) for sorting
			edgeList.push_back(std::make_tuple(weights[i],neighbors[i],it->first));
		}
	}
	edgeList.sort();//sort in order of increasing weight
	while(!edgeList.empty()){
		if(findK(parent,std::get<2>(edgeList.front())) != findK(parent,std::get<1>(edgeList.front()))){
			//if the verticies are not in the same set add them to the MST graph and put them in the same set
			mst.addEdge(std::get<2>(edgeList.front()),std::get<1>(edgeList.front()),std::get<0>(edgeList.front()));
			unionK(parent,std::get<2>(edgeList.front()),std::get<1>(edgeList.front()));
		}
		edgeList.pop_front();
	}
	
	std::cout << "Kruskal's MST" << std::endl;
	mst.print();
}

int Graph::findK(std::map<int,int>& p, int i){
	if(p.find(i)->second == -1){
		return i;//if vertex i has no parent it is its own parent
	}
	return findK(p,p.find(i)->second);//otherwise find the parent of its parent
}

void Graph::unionK(std::map<int,int>& p, int x, int y){
	int xP = findK(p,x);
	int yP = findK(p,y);
	if(xP != yP){//if the two verticies are not in the same set then put them in the same set
		p.find(xP)->second = yP;
	}
}
