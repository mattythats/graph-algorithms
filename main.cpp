#include "graph.h"
#include <chrono>

int main(){
	std::srand(std::time(0));
	//airport graph taken from
	//http://ww3.algorithmdesign.net/sample/ch07-weights.pdf
	//0 is bos
	//1 is jfk
	//2 is mia
	//3 is ord
	//4 is dfw
	//5 is sfo
	//6 is lax
	//weights are distance in miles
	Graph i(false,true);
	i.addEdge(0,5,2704);
	i.addEdge(0,3,867);
	i.addEdge(0,1,187);
	i.addEdge(0,2,1258);
	i.addEdge(1,3,740);
	i.addEdge(1,2,1090);
	i.addEdge(2,4,1121);
	i.addEdge(2,6,2342);
	i.addEdge(3,4,802);
	i.addEdge(3,5,1846);
	i.addEdge(4,5,1464);	
	i.addEdge(4,6,1235);
	i.addEdge(5,6,337);
	i.print();
	i.dijkstra(1,6); //(1,6) should give us the path 1->3->4->6 with a distance of 2777
	i.bellman_ford(1,6);//this should give us the same results as dijkstra*/
	i.floyd_warshall(1,6);
	std::cout << "-----------------" << std::endl;
	
	Graph fw(true,true);
	fw.addEdge(1,3,-2);
	fw.addEdge(3,4,2);
	fw.addEdge(4,2,-1);
	fw.addEdge(2,1,4);
	fw.addEdge(2,3,3);
	fw.print();
	fw.dijkstra(2,4);
	fw.bellman_ford(2,4);
	fw.floyd_warshall(2,4);
	std::cout << "-----------------" << std::endl;

	Graph p(false,true);
	p.addEdge(0,1,2);
	p.addEdge(0,3,1);
	p.addEdge(1,3,2);
	p.addEdge(2,3,3);
	p.print();
	p.prim();
	p.kruskal();
	std::cout << "-----------------" << std::endl;

	Graph k(false,true);
	k.addEdge(7,6,1);
	k.addEdge(8,2,2);
	k.addEdge(6,5,2);
	k.addEdge(0,1,4);
	k.addEdge(2,5,4);
	k.addEdge(8,6,6);
	k.addEdge(2,3,7);
	k.addEdge(7,8,7);
	k.addEdge(0,7,8);
	k.addEdge(1,2,8);
	k.addEdge(3,4,9);
	k.addEdge(5,4,10);
	k.addEdge(1,7,11);
	k.addEdge(3,5,14);
	k.print();
	k.prim();
	k.kruskal();
	return 0;
}
