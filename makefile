main: main.o graph.o
	g++ -o main main.o graph.o

main.o: main.cpp
	g++ -std=c++11 -c main.cpp

graph.o: graph.cpp graph.h
	g++ -std=c++11 -c graph.cpp
